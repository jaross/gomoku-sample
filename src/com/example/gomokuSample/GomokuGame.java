package com.example.gomokuSample;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import static android.view.View.OnClickListener;

public class GomokuGame extends Activity implements OnClickListener {

    public static final int BOARD_SIZE = 10;

    private Player currentPlayer;
    private int cellSideLength;
    private boolean finished;
    private Cell[][] board = new Cell[BOARD_SIZE][BOARD_SIZE];
    private boolean isAiEnabled;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isAiEnabled = true;
        finished = false;
        currentPlayer = Player.CROSS;

        setContentView(R.layout.main);
        cellSideLength = calculateSideLength();

        TableLayout table = (TableLayout) findViewById(R.id.table);
        table.setBackgroundColor(Color.BLACK);

        for (int row = 0; row < BOARD_SIZE; row++) {
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            for (int column = 0; column < BOARD_SIZE; column++) {
                Cell cell = createCell(new Point(column, row));
                board[row][column] = cell;
                tableRow.addView(cell);
            }
            table.addView(tableRow);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.withComputer:
                isAiEnabled = !isAiEnabled;
                item.setChecked(isAiEnabled);
                break;
            case R.id.reset:
                onCreate(null);
                break;
            case R.id.close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        renderChoice((Cell) view);
    }

    private void renderChoice(Cell cell) {
        if (finished) {
            setNotification(R.string.game_finished);
            return;
        }

        if (cell.isBusy()) {
            setNotification(R.string.busy);
            return;
        }
        cell.setPlayer(currentPlayer);

        if (FinishChecker.checkForWin(board, cell)) {
            setNotification(currentPlayer.winnerNotification());
            finished = true;
            return;
        }

        currentPlayer = currentPlayer.getOpponent();
        setNotification(currentPlayer.nextTurnNotification());
        if (isAiEnabled && currentPlayer == Player.ZERO) {
            renderChoice(Ai.nextTurn(board, cell.getPosition()));
        }
    }

    private void setNotification(int message) {
        ((TextView) findViewById(R.id.notification)).setText(message);
    }

    private Cell createCell(Point position) {
        Cell cell = new Cell(this, position);
        cell.setImageResource(R.drawable.square);
        cell.setMaxHeight(cellSideLength);
        cell.setMaxWidth(cellSideLength);
        cell.setAdjustViewBounds(true);
        cell.setOnClickListener(this);
        return cell;
    }

    private int calculateSideLength() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return Math.min(size.y, size.x) / BOARD_SIZE;
    }
}
