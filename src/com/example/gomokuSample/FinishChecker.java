package com.example.gomokuSample;

import static com.example.gomokuSample.GomokuGame.BOARD_SIZE;

public class FinishChecker {

    private static int CHECK_POINT = 5;

    public static boolean checkForWin(Cell[][] board, Cell currentCell, int... checkPoint) {
        CHECK_POINT = checkPoint.length > 0 ? checkPoint[0] : 5;

        for (Function function : new Function[]{
                checkRow, checkColumn, checkDiagonalLeftUpToRightDown, checkDiagonalRightUpToLeftDown}) {
            if (function.checkCondition(board, currentCell)) {
                return true;
            }
        }

        return false;
    }

    static Function checkRow = new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell currentCell) {
            Cell[] cells = board[currentCell.getPosition().y];
            Player player = currentCell.getPlayer();
            int count = 0;
            for (Cell cell : cells) {
                if (cell.hasPlayer(player)) {
                    count++;
                } else {
                    count = 0;
                    continue;
                }
                if (count == CHECK_POINT) {
                    return true;
                }
            }
            return false;
        }
    };

    static Function checkColumn = new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell currentCell) {
            int xPos = currentCell.getPosition().x;
            Player player = currentCell.getPlayer();
            int count = 0;
            for (int row = 0; row < BOARD_SIZE; row++) {
                if (board[row][xPos].hasPlayer(player)) {
                    count++;
                } else {
                    count = 0;
                    continue;
                }
                if (count == CHECK_POINT) {
                    return true;
                }
            }
            return false;
        }
    };

    static Function checkDiagonalLeftUpToRightDown = new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell currentCell) {
            int xPos = currentCell.getPosition().x;
            int yPos = currentCell.getPosition().y;
            Player player = currentCell.getPlayer();
            int count = 1;
            for (int diag = 1; diag <= 4; diag++) {
                if (xPos - diag < 0 || yPos - diag < 0
                        || !board[yPos - diag][xPos - diag].hasPlayer(player)) {
                    break;
                }
                count++;
                if (count == CHECK_POINT) {
                    return true;
                }
            }
            for (int diag = 1; diag <= 4; diag++) {
                if (xPos + diag >= BOARD_SIZE || yPos + diag >= BOARD_SIZE
                        || !board[yPos + diag][xPos + diag].hasPlayer(player)) {
                    break;
                }
                count++;
                if (count == CHECK_POINT) {
                    return true;
                }
            }
            return false;
        }
    };

    static Function checkDiagonalRightUpToLeftDown = new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell currentCell) {
            int xPos = currentCell.getPosition().x;
            int yPos = currentCell.getPosition().y;
            Player player = currentCell.getPlayer();
            int count = 1;
            for (int diag = 1; diag <= 4; diag++) {
                if (yPos + diag >= BOARD_SIZE || xPos - diag < 0 || !board[yPos + diag][xPos - diag].hasPlayer(player)) {
                    break;
                }
                count++;
                if (count == CHECK_POINT) {
                    return true;
                }
            }
            for (int diag = 1; diag <= 4; diag++) {
                if (xPos + diag >= BOARD_SIZE || yPos - diag < 0 || !board[yPos - diag][xPos + diag].hasPlayer(player)) {
                    break;
                }
                count++;
                if (count == CHECK_POINT) {
                    return true;
                }
            }
            return false;
        }
    };
}
