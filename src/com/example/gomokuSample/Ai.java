package com.example.gomokuSample;

import android.graphics.Point;

import static com.example.gomokuSample.FinishChecker.checkForWin;
import static com.example.gomokuSample.Player.CROSS;
import static com.example.gomokuSample.Player.ZERO;

public class Ai {

    public static final int CLOSE_BY_FACTOR = 20;
    public static final int MAKES_THREE_FACTOR = 50;
    public static final int MAKES_CLOSED_FOUR_FACTOR = 85;

    public static Cell nextTurn(final Cell[][] board, final Point lastPlayed) {
        Iterator it = new Iterator(board);

        Cell bestTurn = it.findBestTurn(checkForWin, checkForBlockingWin, checkForBlockingFour, checkForBlockingThree);

        if (bestTurn != null) {
            return bestTurn;
        }

        Function calculateScores = new Function() {
            @Override
            public boolean checkCondition(Cell[][] board, Cell cell) {  //setting the scores...
                if (cell.isEmpty()) {
                    cell.setPlayer(ZERO);
                    Point point = cell.getPosition();
                    if (checkForWin(board, cell, 4)) {
                        cell.addScore(MAKES_CLOSED_FOUR_FACTOR);
                    }
                    if (checkForWin(board, cell, 3)) {
                        cell.addScore(MAKES_THREE_FACTOR);
                    }
                    cell.removePlayer();
                    cell.addScore((int) (CLOSE_BY_FACTOR / avgSpacesAway(lastPlayed, point)));
                } else {
                    cell.setScore(Integer.MIN_VALUE); //if it's played, don't go there
                }
                return false;
            }
        };
        it.findBestTurn(calculateScores);

        final BestResult result = new BestResult(null, -1);

        it.findBestTurn(and(isCellEmpty, new Function() {
            @Override
            public boolean checkCondition(Cell[][] board, Cell cell) {     //figuring out the best score
                if (cell.getScore() > result.score) {
                    result.pushCell(cell);
                }
                return false;
            }
        }));

        return result.cell;
    }

    private static double avgSpacesAway(Point lastPlayed, Point currentPosition) {
        return Math.sqrt(Math.pow((Math.abs(lastPlayed.x - currentPosition.x)), 2)
                + Math.pow(Math.abs(lastPlayed.y - currentPosition.y), 2));
    }

    private static final Function isCellEmpty = new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell cell) {
            return cell.isEmpty();
        }
    };

    private static Function checkForWin = and(isCellEmpty, new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell cell) {
            cell.setPlayer(ZERO);
            boolean result = checkForWin(board, cell);
            cell.removePlayer();
            return result;
        }
    }
    );

    private static Function checkForBlockingWin = and(isCellEmpty,
            new Function() {
                @Override
                public boolean checkCondition(Cell[][] board, Cell cell) {
                    cell.setPlayer(CROSS);
                    boolean result = checkForWin(board, cell);
                    cell.removePlayer();
                    return result;
                }
            });

    private static Function and(final Function f1, final Function f2) {
        return new Function() {
            @Override
            public boolean checkCondition(Cell[][] board, Cell cell) {
                return f1.checkCondition(board, cell) && f2.checkCondition(board, cell);
            }
        };
    }

    private static Function checkForBlockingThree = and(isCellEmpty, new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell cell) {
            cell.setPlayer(CROSS);
            boolean result = checkForWin(board, cell, 3);
            cell.removePlayer();
            return result;
        }
    });

    private static Function checkForBlockingFour = and(isCellEmpty, new Function() {
        @Override
        public boolean checkCondition(Cell[][] board, Cell cell) {
            cell.setPlayer(CROSS);
            boolean result = checkForWin(board, cell, 4);
            cell.removePlayer();
            return result;
        }
    });

    static class Iterator {
        Cell[][] board;

        Iterator(Cell[][] board) {
            this.board = board;
        }

        Cell findBestTurn(Function... function) {
            for (Function f : function) {
                Cell result = findBestTurn(f);
                if (result != null) {
                    return result;
                }
            }
            return null;
        }

        Cell findBestTurn(Function function) {
            for (Cell[] row : board) {
                for (Cell cell : row) {
                    if (function.checkCondition(board, cell)) {
                        return cell;
                    }
                }
            }
            return null;
        }
    }

    private static class BestResult {
        public Cell cell;
        public int score;

        public BestResult(Cell cell, int score) {
            this.cell = cell;
            this.score = score;
        }

        public void pushCell(Cell cell) {
            this.cell = cell;
            this.score = cell.getScore();
        }
    }
}
