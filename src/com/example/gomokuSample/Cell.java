package com.example.gomokuSample;

import android.content.Context;
import android.graphics.Point;
import android.widget.ImageView;

public class Cell extends ImageView {
    private Player player;
    private Point position;
    private int score;

    public Cell(Context context, Point position) {
        super(context);
        this.position = position;
    }

    public void setPlayer(Player player) {
        this.player = player;
        if (player != null) {
            setImageResource(player.getImage());
        }
    }

    public Player getPlayer() {
        return player;
    }

    public Point getPosition() {
        return position;
    }

    public boolean isBusy() {
        return player != null;
    }

    public boolean isEmpty() {
        return player == null;
    }

    public int getScore() {
        return score;
    }

    public Cell setScore(int score) {
        this.score = score;
        return this;
    }

    public void addScore(int value) {
        this.score += value;
    }

    public void removePlayer() {
        player = null;
        setImageResource(R.drawable.square);
    }

    public boolean hasPlayer(Player player) {
        return this.player == player;
    }
}
