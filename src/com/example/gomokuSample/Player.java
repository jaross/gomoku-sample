package com.example.gomokuSample;

enum Player {
    CROSS {
        @Override
        int getImage() {
            return R.drawable.square_cross;
        }

        @Override
        Player getOpponent() {
            return ZERO;
        }

        @Override
        int nextTurnNotification() {
            return R.string.cross_turn;
        }

        @Override
        int winnerNotification() {
            return R.string.cross_win;
        }
    },
    ZERO {
        @Override
        int getImage() {
            return R.drawable.square_zero;
        }

        @Override
        Player getOpponent() {
            return CROSS;
        }

        @Override
        int nextTurnNotification() {
            return R.string.zero_turn;
        }

        @Override
        int winnerNotification() {
            return R.string.zero_win;
        }
    };

    abstract int getImage();

    abstract Player getOpponent();

    abstract int nextTurnNotification();

    abstract int winnerNotification();
}
