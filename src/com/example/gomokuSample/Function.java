package com.example.gomokuSample;

interface Function {
    boolean checkCondition(Cell[][] board, Cell currentCell);
}
